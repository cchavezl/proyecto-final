require('dotenv').config();
const express = require('express');
const body_parser = require('body-parser');
const request_json = require('request-json');
const app = express();
const port = process.env.PORT || 3000;
const usersFile = require('./users.json');
const URL_BASE = '/techu/v2/';
const URL_mLab = 'https://api.mlab.com/api/1/databases/techu16db/collections/';
const apikey_mlab = 'apiKey='+process.env.API_KEY_MLAB;
const cors = require('cors');  // Instalar dependencia 'cors' con npm
const dateformat = require('dateformat');
const hoy = new Date();

app.listen(port,function(){
console.log('Node JS escuchando en el puerto:' + port);
});
app.use(cors());
app.options('*', cors());

app.use(body_parser.json());

  //Operacion get de accounts con mlab
       app.get(URL_BASE +'users/:id/accounts/:id',
       function(req, res){
         let id = req.params.id;
         const http_client = request_json.createClient(URL_mLab);
         console.log("Cliente http creado correctamente ");
         let field_param = 'f={"_id":0}&';
         let queryString = 'q={"id":'+id+'}&';
         http_client.get('user?'+ field_param+queryString +apikey_mlab,
         function(err, respuesta_Mlab, body){
           console.log('Error:'+err);
           console.log('respuesta_Mlab:'+respuesta_Mlab);
           console.log('Body:'+body);
           var response ={};
           if (err) {
             response = {"msg" : "error al recuperar users de mlab"}
             res.status(500);
           }else {
             if (body.length>0) {
               response=body;
             } else {
               response = {"msg" : "usuario no encontrado"}
               res.status(404);
             }
           }
           res.send(response);
         });
   });


//Method POST login
app.post(URL_BASE + "login",
  function (req, res){
    console.log("POST /techu/v2/login");
    let email = req.body.email;
    let pass = req.body.password;
    let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
    let limFilter = 'l=1&';
    let clienteMlab = request_json.createClient(URL_mLab);
    clienteMlab.get('user?'+ queryString + limFilter + apikey_mlab,
      function(error, respuestaMLab, body) {
        if(!error) {
          if (body.length == 1) { // Existe un usuario que cumple 'queryString'
            let login = '{"$set":{"logged":true}}';
            clienteMlab.put('user?q={"id": ' + body[0].id + '}&' + apikey_mlab, JSON.parse(login),
            //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
              function(errPut, resPut, bodyPut) {
                res.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].id, 'name':body[0].first_name});
                // If bodyPut.n == 1, put de mLab correcto
              });
          }
          else {
            //res.status(404).send({"msg":"Usuario no válido."});
            res.status(200).send({"msg":"Usuario no válido."});
          }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});

//Method POST logout
app.post(URL_BASE + "logout",
  function(req, res) {
    console.log("POST /techu/v2/logout");
    var email = req.body.email;
    var queryString = 'q={"email":"' + email + '","logged":true}&';
    console.log(queryString);
    var  clienteMlab = request_json.createClient(URL_mLab);
    clienteMlab.get('user?'+ queryString + apikey_mlab,
      function(error, respuestaMLab, body) {
        var respuesta = body[0]; // Asegurar único usuario
        if(!error) {
          console.log("respuesta:"+ respuesta);
          if (respuesta != undefined) { // Existe un usuario que cumple 'queryString'
            let logout = '{"$unset":{"logged":true}}';
            clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apikey_mlab, JSON.parse(logout),
            //clienteMlab.put('user/' + respuesta._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
              function(errPut, resPut, bodyPut) {
                res.send({'msg':'Logout correcto', 'user':respuesta.email});
                // If bodyPut.n == 1, put de mLab correcto
              });
            } else {
                res.status(404).send({"msg":"Logout failed!"});
            }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});

//GET CON users
app.get(URL_BASE + 'users',
  function(req, res) {
    console.log("entro.");
    const httpClient = request_json.createClient(URL_mLab);
    console.log("Cliente HTTP mLab creado.");
    const fieldParam = 'f={"_id":0}&';
		//console.log('url:' +URL_mLab+'user?' + fieldParam+ apikey_mlab);
    httpClient.get('user?' + fieldParam + apikey_mlab,
      function(err, respuestaMLab, body) {
        console.log('Error: ' + err);
        console.log('Respuesta MLab: ' + apikey_mlab);
      //  console.log('Body: ' + body);
        var response = {};
        if(err) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      });
});

//POST CON users
app.post(URL_BASE+'users',
    function(req, res){
      console.log('entroooooo');
      const http_client = request_json.createClient(URL_mLab);
      let count_param = 'c=true';
      http_client.get(`user?${count_param}&${apikey_mlab}`,
        function(error, res_mlab, count){
          let newId = count + 1;
          let newUser = {
              "id": newId,
              "first_name": req.body.first_name,
              "last_name": req.body.last_name,
              "email": req.body.email,
              "password": req.body.password
          };
          console.log('entro 1');
          http_client.post(`user?&${apikey_mlab}`, newUser,
            function(error, res_mlab, body){
              console.log('entro 2');
              res.status(201).send(body);
            });
        });
    }
)

//Cinthia

//GET users con id
app.get(URL_BASE + "users/:id",
  function (req, res) {
    var id = req.params.id;
    var fieldParam = 'f={"_id":0}&';
    var queryStringID='q={"id":' + id + '}&';
    console.log("entro a users");
    console.log("id:" + queryStringID);
    var httpClient = request_json.createClient(URL_mLab);
    httpClient.get('user?' + fieldParam + queryStringID + apikey_mlab,
      function(error, respuestaMLab, body){
        console.log('error '+ error);
        console.log('respuestaMLab '+respuestaMLab);
        //console.log('body '+body);
        //var respuesta = body;
        var respuesta = {};
        respuesta = !error ? body : {"msg":"error al recuperar usuario de mlab"};
        res.send(respuesta);
      });
});

//GET CON accounts
app.get(URL_BASE + "accounts",
  function (req, res) {
    console.log("entro a accounts");
    var httpClient = request_json.createClient(URL_mLab);
    const fieldParam = 'f={"_id":0}&';
    console.log("fieldParam:" + fieldParam);
    httpClient.get('account?' + fieldParam + apikey_mlab,
      function(error, respuestaMLab, body){
        console.log('error '+ error);
        console.log('respuestaMLab '+respuestaMLab);
        //console.log('body '+body);
        //var respuesta = body;
        var respuesta = {};
        respuesta = !error ? body : {"msg":"error al recuperar cuentas de mlab"};
        res.send(respuesta);
      });
});

//GET account con id
app.get(URL_BASE + "accounts/:id",
  function (req, res) {
    var id = req.params.id;
    var fieldParam = 'f={"_id":0}&';
    var queryStringID='q={"userId":' + id + '}&';
    console.log("entro a account");
    console.log("fieldParam:" + fieldParam);
    var httpClient = request_json.createClient(URL_mLab);
    httpClient.get('account?' + fieldParam + queryStringID + apikey_mlab,
      function(error, respuestaMLab, body){
        console.log('error '+ error);
        console.log('respuestaMLab '+respuestaMLab);
        //console.log('body '+body);
        //var respuesta = body;
        var respuesta = {};
        respuesta = !error ? body : {"msg":"error al recuperar cuentas de mlab"};
        res.send(respuesta);
      });
});

//GET cuentas de un cliente
app.get(URL_BASE + "users/accounts/:id",
function (req, res) {
  console.log("entró a cuentas de un cliente");
  var id = req.params.id;
  var fieldParam = 'f={"_id":0}&';
  var queryStringID='q={"userId":' + id + '}&';
  var httpClient = request_json.createClient(URL_mLab);
  httpClient.get('account?' + fieldParam + queryStringID + apikey_mlab,
  function(error, respuestaMLab, body){
    console.log(URL_mLab +'account?'+ fieldParam + queryStringID + apikey_mlab);
    var respuesta = body[0];
    if(respuesta!=undefined){
        console.log(JSON.stringify(body))
        var respuesta = {
          "idCuenta":body[0].idCuenta,
          "nroCuenta":body[0].nroCuenta,
          "saldoContable":body[0].saldoContable,
          "saldoDisponible":body[0].saldoDisponible,
          "userId": body[0].userId
        };
        respuesta = !error ? respuesta : {"msg":"Error recuperando cuentas del cliente"};
        res.send(respuesta);

    }else{
        console.log("Cliente no existe");
        res.send({"msg": "Cliente no existe"});
    }
  });
});

//GET de movements
app.get(URL_BASE + "movements",
  function (req, res) {
    console.log("entro a movements");
    var httpClient = request_json.createClient(URL_mLab);
    console.log("cliente http mlab creado")
    const fieldParam = 'f={"_id":0}&';
    console.log("fieldParam:" + fieldParam);
    httpClient.get('movement?' + fieldParam + apikey_mlab,
      function(error, respuestaMLab, body){
        console.log('error '+ error);
        console.log('respuestaMLab '+respuestaMLab);
        //console.log('body '+body);
        //var respuesta = body;
        var respuesta = {};
        respuesta = !error ? body : {"msg":"error al recuperar movimientos de mlab"};
        res.send(respuesta);
      });
});

//GET movimientos de una cuenta
app.get(URL_BASE + "users/accounts/:id/movements",
function (req, res) {
  console.log("entró a movimientos de una cuenta");
  var id = req.params.id;
  var fieldParam = 'f={"_id":0}&';
  var queryStringID='q={"idCuenta":' + id + '}&';
  var httpClient = request_json.createClient(URL_mLab);
  httpClient.get('account?' + fieldParam + queryStringID + apikey_mlab,
  function(error, respuestaMLab, body){
    console.log(URL_mLab +'account?'+ fieldParam + queryStringID + apikey_mlab);
    var respuesta = body[0];
    if(respuesta!=undefined){
        console.log(JSON.stringify(body))
        var respuesta = {
          "idCuenta":body[0].idCuenta,
          "saldoContable":body[0].saldoContable,
          "saldoDisponible":body[0].saldoDisponible,
          "movimientos":body[0].movimientos,
        };
        respuesta = !error ? respuesta : {"msg":"Error recuperando movimientos"};
        res.send(respuesta);

    }else{
        console.log("Cuenta no existe");
        res.send({"msg": "Cuenta no existe"});
    }
  });
});

//GET cuentas de usuario
app.get(URL_BASE + "users/:id/accounts",
function (req, res) {
  console.log("entró a cuentas de usuario");
  var id = req.params.id;
  var fieldParam = 'f={"_id":0}&';
  var queryStringID='q={"userId":' + id + '}&';
  var httpClient = request_json.createClient(URL_mLab);
  httpClient.get('account?' + fieldParam + queryStringID + apikey_mlab,
  function(error, respuestaMLab, body){
    console.log(URL_mLab +'account?'+ fieldParam + queryStringID + apikey_mlab);
    var respuesta = body[0];
    if(respuesta!=undefined){
        console.log(JSON.stringify(body))
        var lista = [];
        var respuesta = {
          "idCuenta":body[0].idCuenta,
          "nroCuenta":body[0].nroCuenta,
          "saldoContable":body[0].saldoContable,
          "saldoDisponible":body[0].saldoDisponible,
          "userid":body[0].userId,
        }
        lista.push(respuesta);
        respuesta = !error ? respuesta : {"msg":"Error"};
        res.send(lista);
    }else{
        console.log("Cuenta no existe");
        res.send({"msg": "Cuenta no existe"});
    }
  });
});

app.post(URL_BASE + 'transfers',
    function(req, res){
      console.log('entró a transfers');
      const http_client = request_json.createClient(URL_mLab);
      let customerId=req.body.customerId;
      let idCuentaCargo=req.body.idCuentaCargo;
      let idCuentaDestino=req.body.idCuentaDestino;
      let nroCuentaDestino=req.body.nroCuentaDestino;
      let montoTransferencia= parseFloat(req.body.montoTransferencia);
      let descTransferencia=req.body.descTransferencia;
      let queryStringCuentaCargo='q={"idCuenta":' + idCuentaCargo + '}&';
      let queryStringCuentaDestino='q={"nroCuenta":"' + nroCuentaDestino + '"}&';
      let httpClient = request_json.createClient(URL_mLab);
      //Se obtiene cuenta de cargo
      httpClient.get('account?' + queryStringCuentaCargo + apikey_mlab,
        function(error, respuestaMLab , bodyCuentaCargo) {
          console.log("entro al post de transferencias");
//          var respuesta = bodyCuentaCargo[0];
          var respuesta = {
            "idCuenta":bodyCuentaCargo.idCuenta,
            "nroCuenta":bodyCuentaCargo.nroCuenta,
            "saldoContable":bodyCuentaCargo.saldoContable,
            "saldoDisponible":bodyCuentaCargo.saldoDisponible,
            "userId": bodyCuentaCargo.userId
          };
          if(respuesta!=undefined){
            console.log("Cuenta Cargo "+bodyCuentaCargo[0].nroCuenta);
            //Obtenemos Cuenta Destino
            httpClient.get('account?'+ queryStringCuentaDestino + apikey_mlab ,
            function(error, respuestaMLab , bodyCuentaDestino) {
//              respuesta = bodyCuentaDestino[0];
              var respuesta = {
                  "idCuenta":bodyCuentaDestino.idCuenta,
                  "nroCuenta":bodyCuentaDestino.nroCuenta,
                  "saldoContable":bodyCuentaDestino.saldoContable,
                  "saldoDisponible":bodyCuentaDestino.saldoDisponible,
                  "userId": bodyCuentaDestino.userId
              };
              if(respuesta!=undefined){
                console.log("Cuenta Destino "+bodyCuentaDestino[0].nroCuenta);
                var saldoCuentaCargo = parseFloat(bodyCuentaCargo[0].saldoDisponible);
                var idCuentaCargo = bodyCuentaCargo[0].idCuenta;
                console.log("saldoCuentaCargo "+saldoCuentaCargo);
                if(montoTransferencia<saldoCuentaCargo){
                  var movimientoCuentaCargo=bodyCuentaCargo[0].movimientos;
                  var cantidadMovCuentacargo = parseInt(movimientoCuentaCargo.length);
                  var newMovimientoCargo = {
                            "idmov":cantidadMovCuentacargo+1,
                            "fecha" : dateformat(hoy,'dd/mm/yyyy'),
                            "descripcion" : "Depósito en Cta: "+bodyCuentaDestino[0].nroCuenta,
                            "monto" : montoTransferencia,
                            "itf": montoTransferencia*0.005,
                            "tipomov" : 5
                  };
                  movimientoCuentaCargo.push(newMovimientoCargo);
                  var updateCuentaCargoJson={"movimientos": movimientoCuentaCargo,"saldoDisponible": (saldoCuentaCargo - montoTransferencia)};
                  var updateCuentaCargoSet = '{"$set":' + JSON.stringify(updateCuentaCargoJson) + '}';
                  //Actualizamos Saldo y movimiento Cargo
                 httpClient.put('account?q={"idCuenta": ' + idCuentaCargo+ '}&' + apikey_mlab, JSON.parse(updateCuentaCargoSet),
                  function(errorCargo, respuestaMLabP, bodyCargo) {
                   if(!errorCargo){
                     var saldoCuentaDestino = parseFloat(bodyCuentaDestino[0].saldoDisponible);
                     console.log(URL_mLab+'?q={"idCuenta": ' + bodyCuentaDestino[0].idCuenta + '}&' + apikey_mlab);
                     var movimientoCuentaDestino=bodyCuentaDestino[0].movimientos;
                     var cantidadMovCuentaDestino = parseInt(movimientoCuentaDestino.length);
                     var newMovimientoDestino = {
                               "idmov":cantidadMovCuentaDestino+1,
                               "fecha" : dateformat(hoy,'dd/mm/yyyy'),
                               "descripcion" : "Abono de Cta: "+bodyCuentaCargo[0].nroCuenta,
                               "monto" : montoTransferencia,
                               "itf": montoTransferencia*0.005,
                               "tipomov" : 6
                     };
                     movimientoCuentaDestino.push(newMovimientoDestino);
                     var updateCuentadestinoJson={"movimientos": movimientoCuentaDestino,"saldoDisponible": (saldoCuentaDestino + montoTransferencia)};
                     var updateCuentaDestinoSet = '{"$set":' + JSON.stringify(updateCuentadestinoJson) + '}';
                     //Actualizamos Saldo y movimientos cuenta Destino
                     httpClient.put('account?q={"idCuenta": ' + bodyCuentaDestino[0].idCuenta + '}&' + apikey_mlab, JSON.parse(updateCuentaDestinoSet),
                      function(errorDestino, respuestaMLabP, bodyDestino) {
                       if(!errorDestino){
                         console.log("Transferencia realizada correctamente");
                         res.send({"estadoTransferencia":"ok","msg": "Transferencia realizada correctamente","saldoDisponibleCuentaCargo":saldoCuentaCargo - montoTransferencia});

                       }else{
                         console.log("Error al realizar la transferencia");
                         res.send({"estadoTransferencia":"error","msg": "Error al realizar la transferencia"});
                       }
                     });
                   }else{
                     console.log("Error al realizar el cargo");
                     res.send({"estadoTransferencia":"error","msg": "Error al realizar el cargo"});
                   }
                 });

               }else{
                 console.log("Saldo insuficiente");
                 res.send({"estadoTransferencia":"error","msg": "Saldo insuficiente"});
               }

             }else{
               console.log("No existe la cuenta de destino");
               res.send({"estadoTransferencia":"error","msg": "No existe la cuenta de destino"});
             }
           });

         }else{
           console.log("No existe la cuenta de cargo");
           res.send({"estadoTransferencia":"error","msg": "No existe la cuenta de cargo"});
         }
       });
   });
